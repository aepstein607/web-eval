const fs = require('fs');
const https = require('https')
const cheerio = require('cheerio')
const { Readable } = require('stream');
const inAgencies = new Readable({
  objectMode: true,
  read: function() {}
});
const stringify = require('csv-stringify');

var parse = function(page) {
  var page = cheerio.load(page);
  page('tr td:first-child a:not(:empty)').each( function (index,el) {
    var agency = page(el).text().replace( /\s+/, ' ' );
    var href = page(el).attr('href');
    // console.log(agency + '=>' + href);
    inAgencies.push(['agency-'+index,agency,href])
    return  true;
  });
}

var stringifier = stringify({
  columns: ['index','agency','href'],
  header: true
});

const listFile = 'agencies.csv'

fs.exists(__dirname+'/'+listFile, function(exists) {
  if (!exists) {
    https.get("https://www.nysl.nysed.gov/ils/nyserver.html",function(res) {
      var page = "";
      res.on('data', function(data) {
        page = page + data;
      }).on('end',function() {
        parse(page);
      }).on('error', function(err) {
        console.error(e);
      });
    });

    var writer = fs.createWriteStream(__dirname+'/'+listFile);

    inAgencies.pipe(stringifier).pipe(writer);
  }
});
