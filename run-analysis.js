module.exports.runAnalysis = function( inFile, idHeading, hrefHeading ) {
  var fs = require('fs');
  var parse = require('csv-parse');
  var transform = require('stream-transform');

  var output = [];
  var parser = parse({columns: true})
  var input = fs.createReadStream(__dirname+'/'+inFile+'.csv');


  //
  const lighthouse = require('lighthouse');
  const chromeLauncher = require('chrome-launcher');

  function launchChromeAndRunLighthouse(url, opts, config = null) {
    return chromeLauncher.launch({chromeFlags: opts.chromeFlags}).then(chrome => {
      opts.port = chrome.port;
      return lighthouse(url, opts, config).then(results => {
        // use results.lhr for the JS-consumeable output
        // https://github.com/GoogleChrome/lighthouse/blob/master/typings/lhr.d.ts
        // use results.report for the HTML/JSON/CSV output as a string
        // use results.artifacts for the trace/screenshots/other specific case you need (rarer)
        return chrome.kill().then(() => results.report)
      });
    });
  }

  const opts = {
    output: 'json'
  };
  //


  var transformer = transform(function(record, callback){
    // If the record has a website, seek test results
    if (record[hrefHeading]) {
      var file = __dirname+'/data/'+record[idHeading]+'.json'
      fs.exists( file, (exists) => {
        if (exists) {
          return callback(null, [record[idHeading],record[hrefHeading]].join('|')+'(already captured)\n');
        }
        else {
          launchChromeAndRunLighthouse(record[hrefHeading],opts).then( results => {
            fs.writeFile(file, results, (err) => {
              if (err) {
                return callback(null, [record[idHeading],record[hrefHeading]].join('|')+'(error writing file)\n');
              }
              else {
                return callback(null, [record[idHeading],record[hrefHeading]].join('|')+'\n');
              }
            });
          })
          .catch( (err) => {
            fs.writeFile(file, '{}', (err) => {
              return callback(null, [record[idHeading],record[hrefHeading]].join('|')+'(error running report)\n');
            });
          });
        }
      });
    }
    else {
      return callback(null, [record[idHeading],record[hrefHeading]].join('|')+'(no website)\n');
    }
  }, {parallel: 10});

  input.pipe(parser).pipe(transformer).pipe(process.stdout);
}
