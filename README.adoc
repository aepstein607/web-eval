= Website Comparison Project

== Introduction

This project attempts to answer the following question:
What are the relative strengths and weaknesses of one government web platform relative to existing classes of websites?
Specifically, it compares a variety of classes of municipal websites to a common benchmark, New York State government websites that have been migrated to the state's cloud-based Drupal platform.
It arose to test the idea that municipalities might be able to realize benefits in terms of website quality by migrating to the state platform or something similar to it.
This project is shared in the hopes that it can be critiqued for validity as an evaluation tool.
Comments and feedback are welcome.

== Methods

The project contains a number of scripts that automate most data collection steps.

* collect-data:
  Obtains a list of localities from OpenNY, including the URL of websites, and stores as localities.csv.
  The file required some modification to correct URLs that were no longer accurate.
* collect-agencies.js:
  Obtains an HTML list of state agencies and websites from NYS Education Department.
  Data are extracted and saved to agencies.csv.
  File was then modified to add drupal and duplicate columns.
  A 1 in the drupal column indicates a site that appears to be migrated to the state's Drupal platform.
  A 1 in the duplicate column indicates a site that is already covered by an earlier entry.
  Some state agencies share a website, so in those cases we count only one instance of the website.
* run-analysis.js:
  Contains a function that iterates over a CSV file of unique identifiers and URLs and runs tests.
  The results of each test are dumped in json format to a file in the data directory.
  Tests are run using the lighthouse nodejs library.
  Up to 10 tests are run in parallel.
  Tests are skipped for any item for which existing data is stored in the data folder.
* collect-data:
  Calls run-analysis.js on localities.csv and agencies.csv to collect data on sites listed in each.
* remove-data:
  Removes existing data outputs from run-analysis.js that are stored in the data folder.
* compile_results.js:
  Iterates over results in the data directory and outputs summary score information to localities_scores.csv.
  The CSV output includes the unique identifier for each test and the results on the five major test categories conducted by lighthouse.
  Each score is on a 0 to 1 scale, with 1 being the best and 0 being the worst.
* analyze_scores.R:
  Iterates over localities_scores.csv and performs statistical analysis to compare each locality class to drupal agency sites.
  Uses difference of means t test to compare.
  For each category, it reports high and low range of 95% confidence interval for difference of means and a p value.
  It also prints out any statistically signficant differences with p < 0.05.
  Results are stored in results.csv.

=== A Note on Municipal Classes

The analysis compares two types of municipal classes with state agencies.
The first comparison is based on municipal forms defined by state law: city, town, village, and county.
This form of municipal classification is problematic, because the form of government is a flawed predictor of the municipality's size, resources, and capabilities.
An alternative classification, which we call logical municipal type, is used.
It was obtained from https://www.osc.state.ny.us/localgov/pubs/research/munistructures.pdf[Outdated Municipal Structures], a report prepared by the Office of State Comptroller, and it better reflects the attributes of the municipality that would broadly determine its web development capacities.

== Results

Summary result CSV's are included for illustration and because they represent a relatively small amount of data.
