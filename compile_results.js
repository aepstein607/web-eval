var fs = require('fs');
var parse = require('csv-parse');
var transform = require('stream-transform');
const { Readable } = require('stream');
const inFiles = new Readable({
  objectMode: true,
  read: function() {}
})
const stringify = require('csv-stringify');

fs.readdir(__dirname+'/data',function(err, files) {
  files.forEach( file => {
    if ( file.search( /\.json/ ) == (file.length - 5) ) {
      // inFiles.push( "\"" + file + "\"" + "\n" )
      inFiles.push(file)
    }
  });
  inFiles.push(null);
});

const categories = ["performance","pwa","accessibility","best-practices","seo"];

var parser = parse({columns: true});
var extractor = transform(function(file, callback){
  fs.readFile(__dirname+'/data/'+file,function(err,raw){
    var data = JSON.parse(raw);
    if ( data["categories"] ) {
      var out = [file.substring(0,file.length-5)]
      categories.forEach( function (category) {
        out.push(data["categories"][category]["score"]);
      });
      return callback(null,out);
    }
    console.log("Problem with " + file);
    return callback(null, null);
  });
});
var stringifier = stringify({
  columns: ["swis"].concat(categories),
  header: true
});

var writer = fs.createWriteStream(__dirname+"/localities_scores.csv")

inFiles.pipe(extractor).pipe(stringifier).pipe(writer);
